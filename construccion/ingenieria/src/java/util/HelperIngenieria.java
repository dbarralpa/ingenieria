package util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import model.ModActividad;
import model.ModEncargado;
import model.ModSesion;
import model.ModActividad;

/**
 *
 * @author dbarra
 */
public class HelperIngenieria {

    public static ModEncargado encargado(ArrayList encargados) {
        ModEncargado encargado = new ModEncargado();
        for (int i = 0; i < encargados.size(); i++) {
            ModEncargado encargado1 = (ModEncargado) encargados.get(i);
            if (encargado1.getChecked().equals("SI")) {
                encargado = encargado1;
                break;
            }
        }
        return encargado;
    }

    public static Timestamp fechaEntrega(String fecha) {
        return new Timestamp(java.sql.Date.valueOf(fecha.substring(6, 10) + "-" + fecha.substring(3, 5) + "-" + fecha.substring(0, 2)).getTime());
    }

    public static String minutosTareas(ArrayList procesos, int idTarea, ArrayList equiposIngenieria) {
        String html = "";
        for (int i = 0; i < procesos.size(); i++) {
            ModActividad act = (ModActividad) procesos.get(i);
            boolean est = false;
            for (int a = 0; a < equiposIngenieria.size(); a++) {
                ModActividad acti = (ModActividad) equiposIngenieria.get(a);
                if (act.getAno() == acti.getAno() && act.getMes() == acti.getMes() && acti.getIdActividad() == idTarea) {
                    est = true;
                    long f = acti.getMinutoReal() * 60000l;
                    long diffMinutos = Math.abs(f / (60 * 1000));
                    long restominutos = diffMinutos % 60;

                    // calcular la diferencia en horas
                    long diffHoras = (f / (60 * 60 * 1000));
                    html += "<td style='background-color:#CCC'>" + String.valueOf(diffHoras + ":" + restominutos) + "</a></td>\n";
                    html += "<td style='background-color:#CCC'><a href='../FrmIngenieria?id=" + acti.getIdActividad() + "&mes=" + acti.getMes() + "&ano=" + acti.getAno() + "&detalleTarea=" + a + "'/>" + acti.getCount() + "</a></td>\n";
                }
            }
            if (!est) {
                html += "<td>&nbsp</td>\n";
                html += "<td>&nbsp</td>\n";
            }
        }
        return html;
    }

    public static String minutosActividadesPorTarea(ArrayList procesos, ModActividad acti1, ArrayList actividades) {
        String html = "";
        for (int i = 0; i < procesos.size(); i++) {
            ModActividad act = (ModActividad) procesos.get(i);
            boolean est = false;
            for (int a = 0; a < actividades.size(); a++) {
                ModActividad acti = (ModActividad) actividades.get(a);
                if (act.getAno() == acti.getAno() && act.getMes() == acti.getMes() && acti.getNombre().equals(acti1.getNombre())) {
                    est = true;
                    long f = acti.getMinutoReal() * 60000l;
                    long diffMinutos = Math.abs(f / (60 * 1000));
                    long restominutos = diffMinutos % 60;

                    // calcular la diferencia en horas
                    long diffHoras = (f / (60 * 60 * 1000));
                    html += "<td class='Estilo1'>" + String.valueOf(diffHoras + ":" + restominutos) + "</a></td>\n";
                    html += "<td class='Estilo1'><a href='../FrmIngenieria?id=" + acti.getIdActividad() + "&mes=" + acti.getMes() + "&ano=" + acti.getAno() + "&detalleActividad=" + a + "'/>" + acti.getCount() + "</a></td>\n";
                }
            }
            if (!est) {
                html += "<td>&nbsp</td>\n";
                html += "<td>&nbsp</td>\n";
            }
        }
        return html;
    }

    public static long minutosActividad(String dia, String ano, String mes, String horaIni, String horaFin) {

        Timestamp horasIni = Timestamp.valueOf(ano + "-" + horaSeteada(mes) + "-" + horaSeteada(dia) + " " + horaIni + ":00");
        Timestamp horasFin = Timestamp.valueOf(ano + "-" + horaSeteada(mes) + "-" + horaSeteada(dia) + " " + horaFin + ":00");
        long val = horasFin.getTime() - horasIni.getTime();
        val = (val / 60000);
        return val;
    }

    public static boolean isFechaInvalida(String dia, String ano, String mes, String horaIni, String horaFin) {

        Timestamp horasIni = Timestamp.valueOf(ano + "-" + horaSeteada(mes) + "-" + horaSeteada(dia) + " " + horaIni + ":00");
        Timestamp horasFin = Timestamp.valueOf(ano + "-" + horaSeteada(mes) + "-" + horaSeteada(dia) + " " + horaFin + ":00");
        long val = horasFin.getTime() - horasIni.getTime();
        val = (val / 60000);
        if (val < 0) {
            return true;
        } else {
            return false;
        }

    }

    public static String horaSeteada(String hora) {
        String hor = "";
        if (String.valueOf(hora).length() == 1) {
            hor = "0" + hora;
        } else {
            hor = hora;
        }
        return hor;
    }

    public static String devolverHoras(int minutos) {
        long f = minutos * 60000l;
        long diffMinutos = Math.abs(f / (60 * 1000));
        long restominutos = diffMinutos % 60;

        // calcular la diferencia en horas
        long diffHoras = (f / (60 * 60 * 1000));
        return String.valueOf(diffHoras + ":" + restominutos);
    }

    public static String mes(int mes) {
        String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        return meses[mes - 1];
    }

    public static Boolean buscaCoincidencia(ModActividad acti, String buscar) {
        if (acti.getNombre().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || acti.getNp().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || String.valueOf(acti.getDia()).indexOf(buscar) > -1
                || acti.getHoraIni().indexOf(buscar) > -1
                || acti.getHoraFin().indexOf(buscar) > -1
                || HelperIngenieria.devolverHoras(acti.getMinutoReal()).indexOf(buscar) > -1
                || acti.getHoraIniExtra().indexOf(buscar) > -1
                || HelperIngenieria.devolverHoras(acti.getMinutoRealExtra()).indexOf(buscar) > -1
                || String.valueOf(acti.getCantidadDoc()).indexOf(buscar) > -1
                || String.valueOf(acti.getNumRevisionPlanos()).indexOf(buscar) > -1
                || acti.getMotivoRevision().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || acti.getObservacion().toLowerCase().indexOf(buscar.toLowerCase()) > -1) {
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList ordenarArreglo(ArrayList arreglo, int col, String dir) {
        if (dir.equals("asc")) {
            if (col == 0){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getNombre().compareTo(acti2.getNombre());}});}
            if (col == 1){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getNp().compareTo(acti2.getNp());}});}
            if (col == 2){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti1.getDia()).compareTo(new Integer(acti2.getDia()));}});}
            if (col == 3){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getHoraIni().compareTo(acti2.getHoraIni());}});}
            if (col == 4){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getHoraFin().compareTo(acti2.getHoraFin());}});}
            if (col == 5){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return HelperIngenieria.devolverHoras(acti1.getMinutoReal()).compareTo(HelperIngenieria.devolverHoras(acti2.getMinutoReal()));}});}
            if (col == 6){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getHoraIniExtra().compareTo(acti2.getHoraIniExtra());}});}
            if (col == 7){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getHoraFinExtra().compareTo(acti2.getHoraFinExtra());}});}
            if (col == 8){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return HelperIngenieria.devolverHoras(acti1.getMinutoRealExtra()).compareTo(HelperIngenieria.devolverHoras(acti2.getMinutoRealExtra()));}});}
            if (col == 9){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti1.getCantidadDoc()).compareTo(new Integer(acti2.getCantidadDoc()));}});}
            if (col == 10){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti1.getNumRevisionPlanos()).compareTo(new Integer(acti2.getNumRevisionPlanos()));}});}            
            if (col == 11){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getMotivoRevision().toLowerCase().compareTo(acti2.getMotivoRevision().toLowerCase());}});}
            if (col == 12){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti1.getObservacion().toLowerCase().compareTo(acti2.getObservacion().toLowerCase());}});}                        
        } else {
            if (col == 0){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getNombre().compareTo(acti1.getNombre());}});}
            if (col == 1){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getNp().compareTo(acti1.getNp());}});}
            if (col == 2){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti2.getDia()).compareTo(new Integer(acti1.getDia()));}});}
            if (col == 3){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getHoraIni().compareTo(acti1.getHoraIni());}});}
            if (col == 4){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getHoraFin().compareTo(acti1.getHoraFin());}});}
            if (col == 5){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return HelperIngenieria.devolverHoras(acti2.getMinutoReal()).compareTo(HelperIngenieria.devolverHoras(acti1.getMinutoReal()));}});}
            if (col == 6){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getHoraIniExtra().compareTo(acti1.getHoraIniExtra());}});}
            if (col == 7){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getHoraFinExtra().compareTo(acti1.getHoraFinExtra());}});}
            if (col == 8){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return HelperIngenieria.devolverHoras(acti2.getMinutoRealExtra()).compareTo(HelperIngenieria.devolverHoras(acti1.getMinutoRealExtra()));}});}
            if (col == 9){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti2.getCantidadDoc()).compareTo(new Integer(acti1.getCantidadDoc()));}});}
            if (col == 10){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return new Integer(acti2.getNumRevisionPlanos()).compareTo(new Integer(acti1.getNumRevisionPlanos()));}});}            
            if (col == 11){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getMotivoRevision().toLowerCase().compareTo(acti1.getMotivoRevision().toLowerCase());}});}
            if (col == 12){Collections.sort(arreglo, new Comparator<ModActividad>(){@Override public int compare(ModActividad acti1, ModActividad acti2){return acti2.getObservacion().toLowerCase().compareTo(acti1.getObservacion().toLowerCase());}});}                        
        }
        return arreglo;
    }
}

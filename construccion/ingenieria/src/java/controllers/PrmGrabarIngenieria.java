package controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.ModActividad;
import model.ModEquipo;
import model.ModSesion;
import sql.ConexionSQL;
import util.HelperIngenieria;

public class PrmGrabarIngenieria {

    static SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");

    public static boolean grabarPedidoMateriales(ModSesion Sesion) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estadoDet1 = false;
        boolean estadoDet2 = false;
        String ccc = null;
        String vvv = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                Date date = new Date();
                ccc = "nb_id_tarea,d_fecha_tarea,vc_usuario";
                vvv = Sesion.getProceso() + "�";
                vvv += new Timestamp(date.getTime()) + "�";
                vvv += Sesion.getUsuario().getUsuario();

                if (cn.InsertReg("TBL_SIS_TAREA", ccc, vvv)) {
                    estado = true;
                } else {
                    estado = false;
                }
                if (estado) {
                    if (grabarEquiposTarea(cn, Sesion)) {
                        estadoDet1 = true;
                        if (grabarActividadEncargadoTarea(cn, Sesion)) {
                            estadoDet2 = true;
                        } else {
                            estadoDet2 = false;
                        }
                    } else {
                        estadoDet1 = false;
                    }
                }
                cn.commitSQL(estadoDet1 && estado && estadoDet2);
            }

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        } finally {
            cn.closeSQL();
        }
        return estado && estadoDet1 && estadoDet2;
    }

    private static boolean grabarEquiposTarea(ConexionSQL cn, ModSesion Sesion) throws SQLException {
        boolean estadoDET = false;
        String ccc = null;
        String vvv = "nb_id_tarea,nb_pedido,nb_linea";
        for (int a = 0; a < Sesion.getEquiposAnalizados().size(); a++) {
            ModEquipo equipo = (ModEquipo) Sesion.getEquiposAnalizados().get(a);
            if (equipo.getChecked().equals("checked")) {
                ccc = Sesion.getProceso() + "�";
                ccc += equipo.getNp() + "�";
                ccc += equipo.getItem();
                estadoDET = cn.InsertReg("TBL_SIS_EQUIPO_TAREA", vvv, ccc);
                if (!estadoDET) {
                    break;
                }
            }
        }

        return estadoDET;
    }

    private static boolean grabarActividadEncargadoTarea(ConexionSQL cn, ModSesion Sesion) throws SQLException {
        boolean estadoDET = false;
        String ccc = null;
        String vvv = "nb_id_actividad,nb_id_encargado,nb_id_tarea,d_fecha_entrega";
        for (int a = 0; a < Sesion.getActividades().size(); a++) {
            ModActividad actividad = (ModActividad) Sesion.getActividades().get(a);
            if (actividad.getChecked().trim().equals("checked")) {
                ccc = actividad.getId() + "�";
                ccc += actividad.getEncargado().getId() + "�";
                ccc += Sesion.getProceso() + "�";
                ccc += actividad.getFechaEntrega();
                estadoDET = cn.InsertReg("TBL_SIS_ACTIVIDAD_ENCARGADO_TAREA", vvv, ccc);
                if (!estadoDET) {
                    break;
                }
            }
        }
        return estadoDET;
    }

    public static boolean actualizarTareas(ArrayList tareas) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estadoDet1 = false;
        boolean estadoDet2 = false;
        int a = 0;

        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {

                for (int i = 0; i < tareas.size(); i++) {

                    ModActividad act = (ModActividad) tareas.get(i);
                    String dia = "";
                    if (String.valueOf(act.getDia()).length() == 1) {
                        dia = "0" + act.getDia();
                    } else {
                        dia = "" + act.getDia();
                    }
                    a = act.getIdActividad();

                    Timestamp horasIni = Timestamp.valueOf("2014-02-" + dia + " " + act.getHoraIniExtra() + ":00");
                    Timestamp horasFin = Timestamp.valueOf("2014-02-" + dia + " " + act.getHoraFinExtra() + ":00");
                    long val = horasFin.getTime() - horasIni.getTime();
                    val = (val / 60000);
                    //if (cn.UpdateReg("TBL_ING_DETALLE_ACTIVIDAD_DET", "vc_hora_real", "" + val, "nb_id=" + act.getIdActividad())) {
                    if (cn.UpdateReg("TBL_ING_DETALLE_ACTIVIDAD_DET", "vc_hora_real_extra", "" + val, "nb_id=" + act.getIdActividad())) {
                        estado = true;
                    } else {
                        estado = false;
                        break;
                    }
                }
                cn.commitSQL(estado);
            }

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        } finally {
            cn.closeSQL();
        }
        return estado && estadoDet1 && estadoDet2;
    }

    public static boolean grabarActividadEnc(ModActividad acti, int ano, int mes, String usuario, boolean est) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estado1 = false;
        String ccc = null;
        String vvv = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                int max = PrmIngenieria.maxActividadEnc(cn);
                //int max = 100;
                ccc = "nb_id_detalle_actividad_enc, vc_usuario, nb_mes, nb_ano";

                vvv = max + "�";
                vvv += usuario + "�";
                vvv += mes + "�";
                vvv += ano;
                if (est) {
                    estado = true;
                    acti.setId(max);
                    estado1 = grabarActividadDet(acti, cn, ano, mes, est);
                } else {
                    if (cn.InsertReg("TBL_ING_DETALLE_ACTIVIDAD_ENC", ccc, vvv)) {
                        estado = true;
                        acti.setId(max);
                        estado1 = grabarActividadDet(acti, cn, ano, mes, est);
                    } else {
                        estado = false;
                    }
                }

                cn.commitSQL(estado && estado1);
            }

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        } finally {
            cn.closeSQL();
        }
        return estado && estado1;
    }

    private static boolean grabarActividadDet(ModActividad acti, ConexionSQL cn, int ano, int mes, boolean est) throws SQLException, Exception {
        boolean estado = false;

        String ccc = "vc_np, nb_dia, vc_hora_ini, vc_hora_fin, vc_hora_real, vc_hora_ini_extra, vc_hora_fin_extra, vc_hora_real_extra, nb_cantidad_documentos, "
                + "nb_num_revision_planos, vc_motivo_revision, vc_observaciones, nb_id_detalle_actividad_enc, nb_id_actividad";

        if (est) {
            String vvv = acti.getNp() + "�";
            vvv += acti.getDia() + "�";
            vvv += acti.getHoraIni() + "�";
            vvv += acti.getHoraFin() + "�";
            vvv += HelperIngenieria.minutosActividad(String.valueOf(acti.getDia()), String.valueOf(ano), String.valueOf(mes), acti.getHoraIni(), acti.getHoraFin()) + "�";
            vvv += acti.getHoraIniExtra() + "�";
            vvv += acti.getHoraFinExtra() + "�";
            vvv += HelperIngenieria.minutosActividad(String.valueOf(acti.getDia()), String.valueOf(ano), String.valueOf(mes), acti.getHoraIniExtra(), acti.getHoraFinExtra()) + "�";
            vvv += acti.getCantidadDoc() + "�";
            vvv += acti.getNumRevisionPlanos() + "�";
            vvv += acti.getMotivoRevision() + "�";
            vvv += acti.getObservacion() + "�";
            vvv += acti.getIdEnc() + "�";
            vvv += acti.getIdActividad();
            if (cn.UpdateReg("TBL_ING_DETALLE_ACTIVIDAD_DET", ccc, vvv, "nb_id = " + acti.getIdDetalleActividad())) {
                estado = true;
            } else {
                estado = false;
            }
        } else {
            String vvv = acti.getNp() + "�";
            vvv += acti.getDia() + "�";
            vvv += acti.getHoraIni() + "�";
            vvv += acti.getHoraFin() + "�";
            vvv += HelperIngenieria.minutosActividad(String.valueOf(acti.getDia()), String.valueOf(ano), String.valueOf(mes), acti.getHoraIni(), acti.getHoraFin()) + "�";
            vvv += acti.getHoraIniExtra() + "�";
            vvv += acti.getHoraFinExtra() + "�";
            vvv += HelperIngenieria.minutosActividad(String.valueOf(acti.getDia()), String.valueOf(ano), String.valueOf(mes), acti.getHoraIniExtra(), acti.getHoraFinExtra()) + "�";
            vvv += acti.getCantidadDoc() + "�";
            vvv += acti.getNumRevisionPlanos() + "�";
            vvv += acti.getMotivoRevision() + "�";
            vvv += acti.getObservacion() + "�";
            vvv += acti.getId() + "�";
            vvv += acti.getIdActividad();
            if (cn.InsertReg("TBL_ING_DETALLE_ACTIVIDAD_DET", ccc, vvv)) {
                estado = true;
            } else {
                estado = false;
            }
        }
        return estado;
    }

    public static boolean deleteActividad(ModActividad acti) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estado1 = false;

        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {

                if (cn.DeleteReg("TBL_ING_DETALLE_ACTIVIDAD_DET", "nb_id = " + acti.getIdDetalleActividad())) {
                    estado = true;
                    estado1 = cn.DeleteReg("TBL_ING_DETALLE_ACTIVIDAD_ENC", "nb_id_detalle_actividad_enc = " + acti.getIdEnc());
                } else {
                    estado = false;
                }

                cn.commitSQL(estado && estado1);
            }

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        } finally {
            cn.closeSQL();
        }
        return estado && estado1;
    }

    public static boolean actualizarReclamo(String observacion, String usuario, long id, int est, String proyectista) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        String vvv = "";
        String ccc = "";
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC)) {
                ccc = "vc_observacion_solucion, vc_usuario_solucion, dt_fecha_solucion, nb_id_estado, vc_proyectista";
                vvv = observacion + "�";
                vvv += usuario + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += est + "�";
                vvv += proyectista;
                estado = cn.UpdateReg("TBL_PRO_RECLAMOS_PLANO", ccc, vvv, " nb_id = " + id);
                cn.commitSQL(estado);
                cn.closeSQL();
            }

        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return estado;
    }
}

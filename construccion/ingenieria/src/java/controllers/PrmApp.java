package controllers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class PrmApp {

//    public static String FORMATO_YMD = "yyyy-MM-dd";
    public static String FORMATO_YDM = "yyyy-dd-MM";
    public static String FORMATO_DMY = "dd/MM/yyyy";
    public static String FORMATO_HMS = "HH:mm:ss";
    public static String FILE_ERROR = "C:/logErrorIng.txt";

    public static String verOverTip(String Contenido) {
        return verOverLib("Info. Tip", Contenido, "#E1E1FF", "#000066");
    }

    public static String verOverLib(String titulo, String Contenido) {
        return verOverLib(titulo, Contenido, "#FFFFCC", "#FF9900");
    }

    public static String verOverLib(String Titulo, String Contenido, String fg, String bg) {
        String tmp = "'";
        if (!Contenido.trim().equals("")) {
            Contenido = Contenido.replace(tmp.charAt(0), '�');
            Contenido = Contenido.replace('"', '�');
            Contenido = Contenido.replaceAll("�", "<br>");
            Contenido = Contenido.replaceAll("\n", "<br>");
            Contenido = Contenido.replaceAll("\r", "");
        }else{
            Contenido = "&nbsp";
        }
        return "onMouseOver=" + '"' + "showOverLib('<center>" + Titulo + "</center>','" + Contenido + "', '" + fg + "', '" + bg + "');" + '"' + " OnMouseOut=" + '"' + "return nd();" + '"';
    }

    public static String getFechaActual() {
        return verFecha(new java.util.Date(), FORMATO_YDM);
    }

    public static String getHoraActual() {
        return verFecha(new java.util.Date(), FORMATO_HMS);
    }

    public static String verFecha(Date fecha, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        if (fecha != null) {
            return f.format(fecha);
        } else {
            return "";
        }
    }

    public static String verFecha(String fecha) {
        if (fecha == null) {
            return "";
        } else if (fecha.equals("")) {
            return fecha;
        } else {
            String ano = fecha.substring(0, 4);
            String mes = fecha.substring(4, 6);
            String dia = fecha.substring(6, 8);
            return dia + "/" + mes + "/" + ano;
        }
    }

    public static String verHtmlNull(String dato, int largo) {
        String tmp = verHtmlNull(dato);
        if (tmp.length() > largo) {
            tmp = tmp.substring(0, (largo - 3)) + "...";
        }
        return tmp;
    }

    public static String verHtmlNull(String dato) {
        if (dato != null) {
            if (dato.trim().equals("")) {
                return "&nbsp;";
            } else {
                return dato.trim();
            }
        } else {
            return "&nbsp;";
        }
    }

    public static String verHtmlEstado(String dato) {
        if (dato == null) {
            return "NULO";
        } else if (dato.equals("VIGENT")) {
            return "VIGENTE";
        } else if (dato.equals("NOVIGE")) {
            return "NO VIGENTE";
        } else {
            return "ELIMINADO";
        }
    }

    public static String verValorHtml(double valor) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(4);
        nf.setMinimumFractionDigits(0);
        return nf.format(valor);
    }

    public static void addError(Exception ex) {
        addError(ex, false);
    }

    public static void addError(Exception ex, boolean conTraza) {
        String Err = "";
        if (conTraza) {
            StackTraceElement[] nn = ex.getStackTrace();
            for (int i = 0; i < nn.length; i++) {
                Err += nn[i].toString() + " ";
            }
        }
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(FILE_ERROR, true);
            pw = new PrintWriter(fichero);
            pw.println(PrmApp.getFechaActual() + " " + PrmApp.getHoraActual());
            pw.println(ex.toString());
            pw.println(Err);
            pw.println("");
        } catch (Exception e) {
        } finally {
            if (null != fichero) {
                try {
                    fichero.close();
                } catch (Exception e1) {
                }
            }
            try {
            } catch (Exception e2) {
            }
        }
    }

    public static Properties loadProperties(String resourceName, Class cl) {
        Properties properties = new Properties();
        ClassLoader loader = cl.getClassLoader();
        try {
            InputStream in = loader.getResourceAsStream(resourceName);
            if (in != null) {
                properties.load(in);
            }

        } catch (IOException ex) {
            PrmApp.addError(ex, true);
        }
        return properties;
    }
}

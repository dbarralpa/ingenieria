package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ModActividad;
import model.ModEquipo;
import model.ModTarea;
import model.ModUsuario;
import sql.ConexionSQL;

public class PrmIngenieria {

    public PrmIngenieria() {
    }

    public static ArrayList tareasIngenieria() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList tareas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " select nb_id,vc_descripcion from TBL_ING_TAREA";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModTarea act = new ModTarea();
                    act.setId(tareas.size());
                    act.setIdTarea(rs.getInt("nb_id"));
                    act.setDescripcion(rs.getString("vc_descripcion"));
                    tareas.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return tareas;
    }

    public static ArrayList traerHorasActividadesTareas() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList tareas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " SELECT nb_id,vc_hora_ini_extra,vc_hora_fin_extra,nb_dia FROM TBL_ING_DETALLE_ACTIVIDAD_DET WHERE nb_id_detalle_actividad_enc = 146";
                //String sql = " SELECT nb_id,vc_hora_ini,vc_hora_fin,nb_dia FROM TBL_ING_DETALLE_ACTIVIDAD_DET WHERE nb_id_detalle_actividad_enc = 146 ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(tareas.size());
                    act.setIdActividad(rs.getInt("nb_id"));
                    act.setHoraIniExtra(rs.getString("vc_hora_ini_extra"));
                    act.setHoraFinExtra(rs.getString("vc_hora_fin_extra"));
                    //act.setHoraIniExtra(rs.getString("vc_hora_ini"));
                    //act.setHoraFinExtra(rs.getString("vc_hora_fin"));
                    act.setDia(rs.getInt("nb_dia"));
                    tareas.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return tareas;
    }

    public static ArrayList traerFechaAnos() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList tareas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " select nb_ano,nb_mes from TBL_ING_DETALLE_ACTIVIDAD_ENC group by nb_ano,nb_mes ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(tareas.size());
                    act.setAno(rs.getInt("nb_ano"));
                    act.setMes(rs.getInt("nb_mes"));
                    tareas.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return tareas;
    }

    public static ArrayList traerDatosAtividadesTareasAno() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList tareas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " SELECT sum(convert(numeric,vc_hora_real)) as minutos,nb_id_descripcion_actividad,nb_mes,nb_ano,count(vc_hora_real) as contar";
                sql += " FROM TBL_ING_DETALLE_ACTIVIDAD_ENC enc";
                sql += " inner join TBL_ING_DETALLE_ACTIVIDAD_DET det";
                sql += " on(det.nb_id_detalle_actividad_enc = enc.nb_id_detalle_actividad_enc)";
                sql += " inner join TBL_ING_ACTIVIDAD act";
                sql += " on(act.nb_id = det.nb_id_actividad)";
                sql += " group by nb_id_descripcion_actividad,nb_mes,nb_ano ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(tareas.size());
                    act.setMinutoReal(rs.getInt("minutos"));
                    act.setIdActividad(rs.getInt("nb_id_descripcion_actividad"));
                    act.setAno(rs.getInt("nb_ano"));
                    act.setMes(rs.getInt("nb_mes"));
                    act.setCount(rs.getInt("contar"));
                    tareas.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return tareas;
    }

    public static ArrayList actividadesPorTarea(int tarea) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList tareas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " select sum(convert(numeric,vc_hora_real)) as minutos,det.nb_id_actividad,act.vc_descripcion,nb_mes,nb_ano,count(*) as contar";
                sql += " from TBL_ING_DETALLE_ACTIVIDAD_DET det";
                sql += " inner join TBL_ING_ACTIVIDAD act";
                sql += " on(det.nb_id_actividad = act.nb_id)";
                sql += " inner join TBL_ING_TAREA tar";
                sql += " on(tar.nb_id = act.nb_id_descripcion_actividad)";
                sql += " inner join TBL_ING_DETALLE_ACTIVIDAD_ENC enc";
                sql += " on(det.nb_id_detalle_actividad_enc = enc.nb_id_detalle_actividad_enc)";
                sql += " where tar.nb_id = " + tarea;
                sql += " group by det.nb_id_actividad,act.vc_descripcion,nb_mes,nb_ano";
                sql += " order by det.nb_id_actividad";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(tareas.size());
                    act.setIdActividad(rs.getInt("nb_id_actividad"));
                    act.setMinutoReal(rs.getInt("minutos"));
                    act.setNombre(rs.getString("vc_descripcion"));
                    act.setMes(rs.getInt("nb_mes"));
                    act.setAno(rs.getInt("nb_ano"));
                    act.setCount(rs.getInt("contar"));
                    tareas.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return tareas;
    }

    public static ArrayList actividadesDetalle(int actividad, int ano, int mes) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList actividades = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                /*String sql = " select enc.vc_usuario,enc.nb_mes,enc.nb_ano,det.vc_np,det.vc_actividad,det.nb_dia,det.vc_hora_ini,";
                 sql += " det.vc_hora_fin,vc_hora_real as minutos,det.vc_hora_ini_extra,det.vc_hora_fin_extra,nb_cantidad_documentos,";
                 sql += " det.nb_num_revision_planos,det.vc_motivo_revision,det.vc_observaciones";
                 sql += " from TBL_ING_DETALLE_ACTIVIDAD_ENC enc";
                 sql += " inner join TBL_ING_DETALLE_ACTIVIDAD_DET det";
                 sql += " on(enc.nb_id_detalle_actividad_enc = det.nb_id_detalle_actividad_enc)";
                 sql += " where nb_mes = " + mes + " and nb_ano = " + ano + " and det.nb_id_actividad = " + actividad;*/

                String sql = "select det.nb_id,enc.nb_mes,enc.nb_ano,det.vc_np,det.vc_np,nombre + ' ' + apellido as vc_usuario,det.nb_dia,det.vc_hora_ini,\n";
                sql += " det.vc_hora_fin,vc_hora_real as minutos,det.vc_hora_ini_extra,det.vc_hora_fin_extra,nb_cantidad_documentos,\n";
                sql += " det.nb_num_revision_planos,det.vc_motivo_revision,det.vc_observaciones,det.nb_id_actividad,enc.nb_id_detalle_actividad_enc,vc_hora_real_extra\n";
                sql += " from TBL_ING_DETALLE_ACTIVIDAD_ENC enc\n";
                sql += " inner join TBL_ING_DETALLE_ACTIVIDAD_DET det\n";
                sql += " on(enc.nb_id_detalle_actividad_enc = det.nb_id_detalle_actividad_enc)\n";
                sql += " inner join TBL_ING_ACTIVIDAD act\n";
                sql += " on(act.nb_id = nb_id_actividad)\n";
                sql += " inner join MAC.dbo.TUsuario";
                sql += " on(login = enc.vc_usuario)";
                sql += " where nb_mes = " + mes + " and nb_ano = " + ano + " and det.nb_id_actividad =" + actividad;

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(actividades.size());
                    act.setIdDetalleActividad(rs.getInt("nb_id"));
                    act.setIdActividad(rs.getInt("nb_id_actividad"));
                    act.setMes(rs.getInt("nb_mes"));
                    act.setAno(rs.getInt("nb_ano"));
                    act.setNp(rs.getString("vc_np"));
                    act.setNombre(rs.getString("vc_usuario"));
                    act.setDia(rs.getInt("nb_dia"));
                    act.setHoraIni(rs.getString("vc_hora_ini"));
                    act.setHoraFin(rs.getString("vc_hora_fin"));
                    act.setMinutoReal(rs.getInt("minutos"));
                    act.setMinutoRealExtra(rs.getInt("vc_hora_real_extra"));
                    act.setHoraIniExtra(rs.getString("vc_hora_ini_extra"));
                    act.setHoraFinExtra(rs.getString("vc_hora_fin_extra"));
                    act.setCantidadDoc(rs.getInt("nb_cantidad_documentos"));
                    act.setNumRevisionPlanos(rs.getInt("nb_num_revision_planos"));
                    act.setMotivoRevision(rs.getString("vc_motivo_revision"));
                    act.setObservacion(rs.getString("vc_observaciones"));
                    act.setIdEnc(rs.getInt("nb_id_detalle_actividad_enc"));
                    actividades.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return actividades;
    }

    public static ArrayList actividadesDetalleTarea(int actividad, int ano, int mes) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList actividades = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = "select det.nb_id,enc.nb_mes,enc.nb_ano,det.vc_np,det.vc_np,nombre + ' ' + apellido as vc_usuario,det.nb_dia,det.vc_hora_ini, ";
                sql += " det.vc_hora_fin,vc_hora_real as minutos,det.vc_hora_ini_extra,det.vc_hora_fin_extra,nb_cantidad_documentos, ";
                sql += " det.nb_num_revision_planos,det.vc_motivo_revision,det.vc_observaciones,det.nb_id_actividad,enc.nb_id_detalle_actividad_enc,vc_hora_real_extra ";
                sql += "from TBL_ING_DETALLE_ACTIVIDAD_ENC enc ";
                sql += "inner join TBL_ING_DETALLE_ACTIVIDAD_DET det ";
                sql += "on(enc.nb_id_detalle_actividad_enc = det.nb_id_detalle_actividad_enc) ";
                sql += "inner join TBL_ING_ACTIVIDAD act ";
                sql += "on(act.nb_id = nb_id_actividad) ";
                sql += "inner join MAC.dbo.TUsuario ";
                sql += "on(login = enc.vc_usuario) ";
                sql += "inner join TBL_ING_TAREA tar ";
                sql += "on(act.nb_id_descripcion_actividad = tar.nb_id) ";
                sql += " where nb_mes = " + mes + " and nb_ano = " + ano + " and act.nb_id_descripcion_actividad =" + actividad;

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(actividades.size());
                    act.setIdDetalleActividad(rs.getInt("nb_id"));
                    act.setIdActividad(rs.getInt("nb_id_actividad"));
                    act.setMes(rs.getInt("nb_mes"));
                    act.setAno(rs.getInt("nb_ano"));
                    act.setNp(rs.getString("vc_np"));
                    act.setNombre(rs.getString("vc_usuario"));
                    act.setDia(rs.getInt("nb_dia"));
                    act.setHoraIni(rs.getString("vc_hora_ini"));
                    act.setHoraFin(rs.getString("vc_hora_fin"));
                    act.setMinutoReal(rs.getInt("minutos"));
                    act.setMinutoRealExtra(rs.getInt("vc_hora_real_extra"));
                    act.setHoraIniExtra(rs.getString("vc_hora_ini_extra"));
                    act.setHoraFinExtra(rs.getString("vc_hora_fin_extra"));
                    act.setCantidadDoc(rs.getInt("nb_cantidad_documentos"));
                    act.setNumRevisionPlanos(rs.getInt("nb_num_revision_planos"));
                    act.setMotivoRevision(rs.getString("vc_motivo_revision"));
                    act.setObservacion(rs.getString("vc_observaciones"));
                    act.setIdEnc(rs.getInt("nb_id_detalle_actividad_enc"));
                    actividades.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return actividades;
    }

    public static ArrayList comboActividades() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList actividades = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " select vc_descripcion, nb_id FROM TBL_ING_ACTIVIDAD";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(actividades.size());
                    act.setNombre(rs.getString("vc_descripcion"));
                    act.setIdActividad(rs.getInt("nb_id"));
                    actividades.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return actividades;
    }

    public static int maxActividadEnc(ConexionSQL cn) throws SQLException, Exception {
        int max = 0;
        String sql = " select MAX(nb_id_detalle_actividad_enc)+1 as maximo FROM TBL_ING_DETALLE_ACTIVIDAD_ENC";
        ResultSet rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            max = rs.getInt("maximo");
        }
        rs.close();
        rs = null;
        return max;
    }

    public static ArrayList actividadesDetallePorUsuario(String usuario, int ano, int mes, int dia) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList actividades = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = " select det.nb_id,enc.nb_mes,enc.nb_ano,det.vc_np,act.vc_descripcion,det.nb_dia,det.vc_hora_ini,";
                sql += " det.vc_hora_fin,vc_hora_real as minutos,det.vc_hora_ini_extra,det.vc_hora_fin_extra,nb_cantidad_documentos,";
                sql += " det.nb_num_revision_planos,det.vc_motivo_revision,det.vc_observaciones,det.nb_id_actividad,enc.nb_id_detalle_actividad_enc,vc_hora_real_extra";
                sql += " FROM TBL_ING_DETALLE_ACTIVIDAD_ENC enc";
                sql += " inner join TBL_ING_DETALLE_ACTIVIDAD_DET det";
                sql += " on(enc.nb_id_detalle_actividad_enc = det.nb_id_detalle_actividad_enc)";
                sql += " inner join TBL_ING_ACTIVIDAD act";
                sql += " on(act.nb_id = nb_id_actividad)";
                sql += " where nb_ano = " + ano + " and nb_mes = " + mes + " and vc_usuario = '" + usuario + "' and nb_dia =" + dia;

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModActividad act = new ModActividad();
                    act.setId(actividades.size());
                    act.setIdDetalleActividad(rs.getInt("nb_id"));
                    act.setIdActividad(rs.getInt("nb_id_actividad"));
                    act.setMes(rs.getInt("nb_mes"));
                    act.setAno(rs.getInt("nb_ano"));
                    act.setNp(rs.getString("vc_np"));
                    act.setNombre(rs.getString("vc_descripcion"));
                    act.setDia(rs.getInt("nb_dia"));
                    act.setHoraIni(rs.getString("vc_hora_ini"));
                    act.setHoraFin(rs.getString("vc_hora_fin"));
                    act.setMinutoReal(rs.getInt("minutos"));
                    act.setMinutoRealExtra(rs.getInt("vc_hora_real_extra"));
                    act.setHoraIniExtra(rs.getString("vc_hora_ini_extra"));
                    act.setHoraFinExtra(rs.getString("vc_hora_fin_extra"));
                    act.setCantidadDoc(rs.getInt("nb_cantidad_documentos"));
                    act.setNumRevisionPlanos(rs.getInt("nb_num_revision_planos"));
                    act.setMotivoRevision(rs.getString("vc_motivo_revision"));
                    act.setObservacion(rs.getString("vc_observaciones"));
                    act.setIdEnc(rs.getInt("nb_id_detalle_actividad_enc"));
                    actividades.add(act);
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();

        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return actividades;
    }

    public static ArrayList reclamosPorPlanos() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList reclamos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC)) {
                /*String sql = " select top 30 nb_id, ch_detencion, nb_np, nb_linea, vc_num_plano, ch_tipo_plano, gen_Desc,nombre + ' ' + apellido as vc_proyectista, vc_observacion, dt_fecha_creacion";
                 sql += " ,case when dt_fecha_solucion is null then (select DATEDIFF(day,dt_fecha_creacion,getDate())) else (select DATEDIFF(day,dt_fecha_creacion,dt_fecha_solucion)) end as dias";
                 sql += " from TBL_PRO_RECLAMOS_PLANO ";
                 sql += " left outer join ENSchaffner.dbo.EE_tabla_generica";
                 sql += " on(vc_area = gen_Codigo and gen_Tipo = 13)";
                 sql += " left outer join MAC.dbo.TUsuario";
                 sql += " on(login = vc_proyectista)";
                 sql += " where nb_id_estado = 1 ";
                 sql += " order by nb_id";*/

                String sql = "select top 30 nb_id, ch_detencion, nb_np, nb_linea, vc_num_plano, ch_tipo_plano, vc_area, vc_observacion, dt_fecha_creacion";
                sql += " ,case when dt_fecha_solucion is null then (select DATEDIFF(day,dt_fecha_creacion,getDate())) else (select DATEDIFF(day,dt_fecha_creacion,dt_fecha_solucion)) end as dias";
                sql += " from TBL_PRO_RECLAMOS_PLANO";
                sql += " where nb_id_estado = 1 ";
                sql += " order by ch_detencion desc,nb_id";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo rec = new ModEquipo();
                    rec.getReclamo().setNumero(rs.getLong("nb_id"));
                    rec.getReclamo().setDetencion(rs.getString("ch_detencion"));
                    rec.setNp(rs.getInt("nb_np"));
                    rec.setItem(rs.getInt("nb_linea"));
                    rec.setNumPlano(rs.getString("vc_num_plano"));
                    rec.setTipoPlano(rs.getString("ch_tipo_plano"));
                    rec.getReclamo().setDetencion(rs.getString("ch_detencion"));
                    rec.getReclamo().setArea(rs.getString("vc_area"));
                    rec.getReclamo().setObservacion(rs.getString("vc_observacion"));
                    rec.getReclamo().setFechaIngreso(rs.getDate("dt_fecha_creacion"));
                    rec.getReclamo().setDias(rs.getInt("dias"));
                    reclamos.add(rec);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return reclamos;
    }

    public static ArrayList proyectista() {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs;
        ArrayList usuarios = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC)) {
                String sql = " SELECT login,nombre,apellido";
                sql += " FROM LG_permiso_a_link ";
                sql += " inner join TUsuario";
                sql += " on(login = Usuario)";
                sql += " WHERE Nombre = 'proyectista'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModUsuario usu = new ModUsuario();
                    usu.setUsuario(rs.getString("login"));
                    usu.setNombre(rs.getString("nombre"));
                    usu.setApellido(rs.getString("apellido"));
                    usuarios.add(usu);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return usuarios;
    }

    public static boolean validarUsuarioALink(String usuario, String nombre) {
        boolean valUsuario = false;
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs;
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC)) {
                String query = "  select Nombre from LG_permiso_a_link where Usuario = '" + usuario + "'";
                rs = cn.ExecuteQuery(query);
                while (rs.next()) {
                    if (rs.getString(1).equals(nombre)) {
                        valUsuario = true;
                        break;
                    }
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return valUsuario;
    }
}

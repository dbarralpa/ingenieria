/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controllers.PrmApp;
import controllers.PrmIngenieria;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;
import model.ModActividad;
import model.ModEquipo;
import model.ModSesion;
import model.ModTarea;
import model.ModUsuario;
import util.HelperIngenieria;

public class VwIngenieria extends PrmApp {

    static SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

    public String grillaTareas(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (Sesion.getDatos().isEmpty()) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                html += "<tr>\n";
                html += "<td style='padding-top: 10px'>\n";
                html += "<table border='1' cellpadding='2' cellspacing='2' class='Contenido'>\n";
                html += "<tr>\n";
                html += "<td>&nbsp;</td>\n";
                for (int i = 0; i < Sesion.getProcesos().size(); i++) {
                    ModActividad acti = (ModActividad) Sesion.getProcesos().get(i);
                    html += "<td colspan='2'>" + acti.getAno() + "-" + HelperIngenieria.mes(acti.getMes()) + "</td>\n";
                }
                html += "</tr>\n";
                for (int i = 0; i < Sesion.getDatos().size(); i++) {
                    ModTarea tar = (ModTarea) Sesion.getDatos().get(i);
                    html += "<tr>\n";
                    html += "<td class='Estilo1' align='left'>\n";
                    html += "<a href='../FrmIngenieria?tarea=" + i + "'/>\n";
                    html += "<img alt='' src='../img/calendar/down.gif' border='0' width='13' height='8' style='CURSOR: pointer'>\n";
                    html += "</a>\n";
                    html += tar.getDescripcion() + "</td>\n";
                    html += HelperIngenieria.minutosTareas(Sesion.getProcesos(), tar.getIdTarea(), Sesion.getEquiposIngenieria()) + "\n";
                    html += "</tr>\n";
                    if (tar.isShowActividad()) {
                        String act = "";
                        for (int a = 0; a < tar.getActividades().size(); a++) {
                            ModActividad acti = (ModActividad) tar.getActividades().get(a);
                            if (!acti.getNombre().equals(act)) {
                                act = acti.getNombre();
                                html += "<tr>";
                                html += "<td class='Estilo1' style=\"padding-left: 20px\">->" + acti.getNombre() + "</td>\n";
                                html += HelperIngenieria.minutosActividadesPorTarea(Sesion.getProcesos(), acti, tar.getActividades()) + "\n";
                                html += "</tr>";
                            }
                        }
                    }
                }
                html += "</table>\n";
                html += "</td>\n";
                html += "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    /* public String grillaActividades(HttpServletRequest req) {
     String html = "";
     ModSesion Sesion = null;
     try {
     Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
     if (Sesion.getAuxiliar().isEmpty()) {
     html = "<tr class='FilaNormal'>\n" + "<td colspan='14' align='center' class=\"ConBorder\">";
     html += "SIN DATOS";
     html += "</td>\n" + "</tr>\n";
     } else {
     //   html += "<tr>\n";
     //   html += "<td>\n";
     //   html += "<table border='1' cellpadding='0' cellspacing='0' class='Contenido'>\n";

     for (int a = 0; a < Sesion.getAuxiliar().size(); a++) {
     ModActividad acti = (ModActividad) Sesion.getAuxiliar().get(a);
     html += "<tr>";
     html += "<td class='Estilo10' " + verOverLib("Ingenier�a", "<b> Nombre usuario: </b><br/>" + acti.getUsuario()) + ">" + verHtmlNull(acti.getUsuario(), 7) + "</td>\n";
     //html += "<td class='Estilo10' style=\"padding-left: 20px\">" + acti.getAno() + "</td>\n";
     //html += "<td class='Estilo10' style=\"padding-left: 20px\">" + acti.getMes() + "</td>\n";
     html += "<td class='Estilo10' " + verOverLib("Ingenier�a", "<b> Np: </b><br/>" + acti.getNp()) + ">" + verHtmlNull(acti.getNp(), 5) + "</td>\n";
     //html += "<td class='Estilo10' style=\"padding-left: 20px\">" + acti.getNombre() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getDia() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getHoraIni() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getHoraFin() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getMinutoReal() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getHoraIniExtra() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getHoraFinExtra() + "</td>\n";
     html += "<td class='Estilo10'>" + 0 + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getCantidadDoc() + "</td>\n";
     html += "<td class='Estilo10'>" + acti.getNumRevisionPlanos() + "</td>\n";
     html += "<td class='Estilo10' " + verOverLib("Ingenier�a", "<b> Motivo de la revisi�n: </b><br/>" + acti.getMotivoRevision().toLowerCase()) + ">" + verHtmlNull(acti.getMotivoRevision().toLowerCase(), 15) + "</td>\n";
     html += "<td class='Estilo10' " + verOverLib("Ingenier�a", "<b> Observaci�n: </b><br/>" + acti.getObservacion().toLowerCase()) + ">" + verHtmlNull(acti.getObservacion().toLowerCase(), 30) + "</td>\n";
     html += "</tr>";
     }

     //  html += "</table>\n";
     //  html += "</td>\n";
     //  html += "</tr>\n";
     }
     } catch (Exception ex) {
     html = "<tr class='FilaNormal'>\n" + "<td colspan='14' align='center' class=\"ConBorder\">";
     html += "</td>\n" + "</tr>\n";
     }
     return html;
     }*/
    public String comboActividades(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbActividad\" id=\"cmbActividad\">";
            html += "<option value='-1'>Actividad</option>";
            for (int i = 0; i < Sesion.getProcesos().size(); i++) {
                ModActividad act = (ModActividad) Sesion.getProcesos().get(i);
                html += "<option value=\"" + act.getIdActividad() + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Sesion.getActividad().getIdActividad() == act.getIdActividad()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += act.getNombre() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboDesdeNormalHora(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbDesdeNormalHora\" id=\"cmbDesdeNormalHora\">";
            html += "<option value='0'>00</option>";
            for (int i = 5; i < 17; i++) {
                html += "<option value=\"" + i + "\"  ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraIni().split(":")[0]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboDesdeNormalMinuto(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbDesdeNormalMinuto\" id=\"cmbDesdeNormalMinuto\">";
            html += "<option value='0'>00</option>";
            for (int i = 0; i < 60; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraIni().split(":")[1]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboHastaNormalHora(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbHastaNormalHora\" id=\"cmbHastaNormalHora\">";
            html += "<option value='0'>00</option>";
            for (int i = 5; i < 17; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraFin().split(":")[0]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboHastaNormalMinuto(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbHastaNormalMinuto\" id=\"cmbHastaNormalMinuto\">";
            html += "<option value='0'>00</option>";
            for (int i = 0; i < 60; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraFin().split(":")[1]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboDesdeExtraHora(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbDesdeExtraHora\" id=\"cmbDesdeExtraHora\">";
            html += "<option value='0'>00</option>";
            for (int i = 16; i < 24; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraIniExtra().split(":")[0]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboDesdeExtraMinuto(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbDesdeExtraMinuto\" id=\"cmbDesdeExtraMinuto\">";
            html += "<option value='0'>00</option>";
            for (int i = 0; i < 60; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraIniExtra().split(":")[1]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboHastaExtraHora(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbHastaExtraHora\" id=\"cmbHastaExtraHora\">";
            html += "<option value='0'>00</option>";
            for (int i = 16; i < 24; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraFinExtra().split(":")[0]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboHastaExtraMinuto(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbHastaExtraMinuto\" id=\"cmbHastaExtraMinuto\">";
            html += "<option value='0'>00</option>";
            for (int i = 0; i < 60; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Integer.parseInt(Sesion.getActividad().getHoraFinExtra().split(":")[1]) == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboCantDoc(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbCantDoc\" id=\"cmbCantDoc\">";
            html += "<option value='0'>0</option>";
            for (int i = 1; i < 30; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Sesion.getActividad().getCantidadDoc() == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboNRevPlanos(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbNRevPlanos\" id=\"cmbNRevPlanos\">";
            html += "<option value='0'>0</option>";
            for (int i = 1; i < 30; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getActividad().getIdDetalleActividad() > 0 && Sesion.getActividad().getNumRevisionPlanos() == i) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboAno(HttpServletRequest req) {
        String html = "";
        Calendar fecha = new GregorianCalendar();
        int ano = fecha.get(Calendar.YEAR);
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name='cmbAno' id='cmbAno'>";
            html += "<option value='-1'>A�o</option>";
            for (int i = ano - 3; i < ano + 3; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getAno() == i) {
                    html += " selected>";
                } else {
                    html += " >";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboMes(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name='cmbMes' id='cmbMes'>";
            html += "<option value='-1'>Mes</option>";
            for (int i = 1; i < 13; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getMes() == i) {
                    html += " selected>";
                } else {
                    html += " >";
                }

                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboDia(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name='cmbDia' id='cmbDia' onchange='submit()'>";
            html += "<option value='-1'>D�a</option>";
            for (int i = 1; i < 32; i++) {
                html += "<option value=\"" + i + "\" ";
                if (Sesion.getDia() == i) {
                    html += " selected>";
                } else {
                    html += " >";
                }
                html += i + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String np(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<input type='text' size='5' name='np' id='np'";
            if (Sesion.getActividad().getIdDetalleActividad() > 0) {
                html += " value='" + Sesion.getActividad().getNp() + "'>";
            } else {
                html += ">";
            }

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String motivoRevision(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<textarea name='motRevision' id='motRevision' cols='10' rows='1'>";
            if (Sesion.getActividad().getIdDetalleActividad() > 0) {
                html += Sesion.getActividad().getMotivoRevision() + "</textarea>";
            } else {
                html += "</textarea>";
            }

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String observacion(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<textarea name='observacion' id='observacion' cols='10' rows='1'>";
            if (Sesion.getActividad().getIdDetalleActividad() > 0) {
                html += Sesion.getActividad().getObservacion() + "</textarea>";
            } else {
                html += "</textarea>";
            }

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String grillaIngresarActividades(HttpServletRequest req) {
        String html = "";
        try {
            html += "<tr>";
            html += "<td class='Estilo10' align='center'>" + np(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboActividades(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboDesdeNormalHora(req) + ":" + comboDesdeNormalMinuto(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboHastaNormalHora(req) + ":" + comboHastaNormalMinuto(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboDesdeExtraHora(req) + ":" + comboDesdeExtraMinuto(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboHastaExtraHora(req) + ":" + comboHastaExtraMinuto(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboCantDoc(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + comboNRevPlanos(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + motivoRevision(req) + "</td>\n";
            html += "<td class='Estilo10' align='center'>" + observacion(req) + "</td>\n";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='11' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String AnoMesDia(HttpServletRequest req) {
        String html = "";
        try {
            html += "<tr>";
            html += "<td class='Estilo10' align='center' style=\"padding-top: 20px\">" + comboAno(req) + "&nbsp;&nbsp;&nbsp;&nbsp;" + comboMes(req) + "&nbsp;&nbsp;&nbsp;&nbsp;" + comboDia(req) + "</td>\n";
            html += "</tr>";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='11' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaActividades(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (Sesion.getAuxiliar().isEmpty()) {
                html = "<tr class='FilaNormal'>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "<td align='center' class=\"ConBorder\">&nbsp;</td>\n";
                html += "</tr>\n";
            } else {

                for (int a = 0; a < Sesion.getAuxiliar().size(); a++) {
                    ModActividad acti = (ModActividad) Sesion.getAuxiliar().get(a);
                    html += "<tr>\n";
                    if (Sesion.getValor().equals("1")) {
                        html += "<td class='Estilo10' align='center' width='171' " + verOverLib("Ingenier�a", "<b> Descripci�n actividad: </b><br/>" + acti.getNombre()) + ">\n";
                        html += "<a href='../FrmIngenieria?actualizarActividad=" + a + "'/>\n";
                        html += verHtmlNull(acti.getNombre(), 30) + "</a></td>\n";
                    } else if (Sesion.getValor().equals("0")) {
                        html += "<td class='Estilo10' align='center' width='171' " + verOverLib("Ingenier�a", "<b> Nombre usuario: </b><br/>" + acti.getNombre()) + ">" + verHtmlNull(acti.getNombre(), 30) + "</a></td>\n";
                    }

                    html += "<td class='Estilo10' align='center' " + verOverLib("Ingenier�a", "<b> Np: </b><br/>" + acti.getNp()) + ">" + verHtmlNull(acti.getNp(), 12) + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getDia() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getHoraIni() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getHoraFin() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + HelperIngenieria.devolverHoras(acti.getMinutoReal()) + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getHoraIniExtra() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getHoraFinExtra() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + HelperIngenieria.devolverHoras(acti.getMinutoRealExtra()) + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getCantidadDoc() + "</td>\n";
                    html += "<td class='Estilo10' align='center'>" + acti.getNumRevisionPlanos() + "</td>\n";
                    html += "<td class='Estilo10' align='center'" + verOverLib("Ingenier�a", "<b> Motivo de la revisi�n: </b><br/>" + acti.getMotivoRevision().toLowerCase()) + ">" + verHtmlNull(acti.getMotivoRevision().toLowerCase(), 15) + "</td>\n";
                    html += "<td class='Estilo10' align='center'" + verOverLib("Ingenier�a", "<b> Observaci�n: </b><br/>" + acti.getObservacion().toLowerCase()) + ">" + verHtmlNull(acti.getObservacion().toLowerCase(), 30) + "</td>\n";
                    html += "</tr>";
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='14' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String encabezadoActividades(HttpServletRequest req) {
        String html = "";
        try {
            html += "<th>Np</th>\n";
            html += "<th>D�a</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Hora inicio") + ">Desde</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Hora t�rmino") + ">Hasta</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Horas trabajadas") + ">Real</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Hora extra inicio") + ">Desde</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Hora extra t�rmino") + ">Hasta</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Horas extras trabajadas") + ">Real</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>Cantidad de documentos") + ">C. Doc</th>\n";
            html += "<th " + verOverLib("Ingenier�a", "<b> Np: </b><br/>N�mero de revisiones") + ">N� R.</th>\n";
            html += "<th>Motivo Revisi�n</th>\n";
            html += "<th>Observaciones</th>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='11' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaReclamosPorPlanos(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>\n";
            html += "<td style='padding-top: 20px' >\n";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td colspan='9' class='Estilo1' align='center'>RECLAMOS VIGENTES</td>";
            html += "</tr>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='80'>Np</td>";
            html += "<td class='Estilo1' width='80'>Item</td>";
            html += "<td class='Estilo1' width='40'>Detenci�n</td>";
            html += "<td class='Estilo1' width='80'>Tipo Plano</td>";
            html += "<td class='Estilo1' width='80'>Area</td>";
            html += "<td class='Estilo1' width='80'>Fecha Ingreso</td>";
            html += "<td class='Estilo1' width='80'>N� Plano</td>";
            html += "<td class='Estilo1' width='80'>D�as atraso</td>";
            html += "<td class='Estilo1' width='300'>Observaci�n</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModEquipo equi = (ModEquipo) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                if (PrmIngenieria.validarUsuarioALink(Sesion.getUsuario().getUsuario(), "reclamosPlanos")) {
                    html += "<td class='Estilo1' width='80'><a href='../FrmIngenieria?esta=2&actualizarEstadoReclamo=" + i + "'/>" + equi.getNp() + "</a></td>";
                } else if (PrmIngenieria.validarUsuarioALink(Sesion.getUsuario().getUsuario(), "reclamosPlanosR")) {
                    html += "<td class='Estilo1' width='80'><a href='../FrmIngenieria?esta=3&actualizarEstadoReclamo=" + i + "'/>" + equi.getNp() + "</a></td>";
                } else {
                    html += "<td class='Estilo1' width='80'>" + equi.getNp() + "</td>";
                }
                html += "<td class='Estilo1' width='80'>" + equi.getItem() + "</td>";
                html += "<td class='Estilo1' width='80'>" + equi.getReclamo().getDetencion() + "</td>";
                html += "<td class='Estilo1' width='80'>" + equi.getTipoPlano() + "</td>";
                html += "<td class='Estilo1' width='80'>" + equi.getReclamo().getArea() + "</td>";
                html += "<td class='Estilo1' width='80'>" + fecha.format(equi.getReclamo().getFechaIngreso()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + equi.getNumPlano() + "</td>";
                html += "<td class='Estilo1' width='80'>" + equi.getReclamo().getDias() + "</td>";
                html += "<td class='Estilo1' width='300' " + PrmApp.verOverLib("Observaci�n", equi.getReclamo().getObservacion()) + ">" + PrmApp.verHtmlNull(equi.getReclamo().getObservacion(), 35) + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='9' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaReclamosPorPlanosEquipo(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>\n";
            html += "<td style='padding-top: 20px' >\n";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td colspan='9' class='Estilo1' align='center'>RECLAMOS VIGENTES</td>";
            html += "</tr>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='80'>Np</td>";
            html += "<td class='Estilo1' width='80'>Item</td>";
            html += "<td class='Estilo1' width='80'>Area</td>";
            html += "<td class='Estilo1' width='80'>Fecha Ingreso</td>";
            html += "<td class='Estilo1' width='80'>N� Plano</td>";
            html += "<td class='Estilo1' width='120'>Proyectista</td>";
            html += "<td class='Estilo1' width='80'>Tipo Plano</td>";
            html += "<td class='Estilo1' width='80'>D�as atraso</td>";
            html += "<td class='Estilo1' width='300'>Observaci�n</td>";
            html += "</tr>";

            ModEquipo equi = Sesion.getEquipo();
            html += "<tr>";
            html += "<td class='Estilo1' width='80'>" + equi.getNp() + "</td>";
            html += "<td class='Estilo1' width='80'>" + equi.getItem() + "</td>";
            html += "<td class='Estilo1' width='80'>" + equi.getReclamo().getArea() + "</td>";
            html += "<td class='Estilo1' width='80'>" + fecha.format(equi.getReclamo().getFechaIngreso()) + "</td>";
            html += "<td class='Estilo1' width='80'>" + equi.getNumPlano() + "</td>";
            html += "<td class='Estilo1' width='120'>" + comboProyectistas(req) + "</td>";
            html += "<td class='Estilo1' width='80'>" + equi.getTipoPlano() + "</td>";
            html += "<td class='Estilo1' width='80'>" + equi.getReclamo().getDias() + "</td>";
            html += "<td class='Estilo1' width='300' " + PrmApp.verOverLib("Observaci�n", equi.getReclamo().getObservacion()) + ">" + PrmApp.verHtmlNull(equi.getReclamo().getObservacion(), 35) + "</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='9' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String comboProyectistas(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbProyectistas\" id=\"cmbProyectistas\">";
            html += "<option value='-1'>Proyectista</option>";
            for (int i = 0; i < Sesion.getUsuarios().size(); i++) {
                ModUsuario usu = (ModUsuario) Sesion.getUsuarios().get(i);
                html += "<option value=\"" + usu.getUsuario() + "\" >";
                html += usu.getNombre() + " " + usu.getApellido() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }
}

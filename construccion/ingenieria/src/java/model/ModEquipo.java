package model;

import java.util.ArrayList;
import java.util.Date;

public class ModEquipo implements Cloneable {

    private int id;
    private int np;
    private int item;
    private String numPlano;
    private String familia;
    private String kva;
    private String serie;
    private String diseno;
    private String status;
    private String tipoPlano;
    private String proyectista;
    private Date expeditacion;
    private Date fechaCliente;
    private String codigoExtructura;
    private String nombreCliente;
    private String checked;
    private ArrayList procesos;
    private boolean analizar;
    private ModReclamo reclamo;

    public ModEquipo() {
        id = 0;
        np = 0;
        item = 0;
        familia = "";
        numPlano = "";
        kva = "";
        serie = "";
        diseno = "";
        status = "";
        tipoPlano = "";
        proyectista = "";
        expeditacion = null;
        fechaCliente = null;
        codigoExtructura = "";
        nombreCliente = "";
        checked = "";
        procesos = new ArrayList();
        analizar = true;
        reclamo = new ModReclamo();
    }

    public String getNumPlano() {
        return numPlano;
    }

    public void setNumPlano(String numPlano) {
        this.numPlano = numPlano;
    }

    public ModReclamo getReclamo() {
        return reclamo;
    }

    public void setReclamo(ModReclamo reclamo) {
        this.reclamo = reclamo;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }

    public String getProyectista() {
        return proyectista;
    }

    public void setProyectista(String proyectista) {
        this.proyectista = proyectista;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiseno() {
        return diseno;
    }

    public void setDiseno(String diseno) {
        this.diseno = diseno;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Date getFechaCliente() {
        return fechaCliente;
    }

    public void setFechaCliente(Date fechaCliente) {
        this.fechaCliente = fechaCliente;
    }

    public boolean isAnalizar() {
        return analizar;
    }

    public void setAnalizar(boolean analizar) {
        this.analizar = analizar;
    }

    public void setProcesos(ArrayList procesos) {
        this.procesos = procesos;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNp() {
        return np;
    }

    public void setNp(int np) {
        this.np = np;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getKva() {
        return kva;
    }

    public void setKva(String kva) {
        this.kva = kva;
    }

    public Date getExpeditacion() {
        return expeditacion;
    }

    public void setExpeditacion(Date expeditacion) {
        this.expeditacion = expeditacion;
    }

    public String getCodigoExtructura() {
        return codigoExtructura;
    }

    public void setCodigoExtructura(String codigoExtructura) {
        this.codigoExtructura = codigoExtructura;
    }

    public ModEquipo copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModEquipo) obj;
    }
}

package model;

public class ModActividad {

    private int id;
    private int idActividad;
    private int idDetalleActividad;
    private int idEnc;
    private String nombre;
    private String checked;
    private String fechaEntrega;
    private String horaIni;
    private String horaFin;
    private String horaIniExtra;
    private String horaFinExtra;
    private String usuario;
    private String np;
    private String motivoRevision;
    private String observacion;
    private int dia;
    private int mes;
    private int ano;
    private int minutoReal;
    private int minutoRealExtra;
    private int count;
    private int cantidadDoc;
    private int numRevisionPlanos;
    private ModEncargado encargado;

    public ModActividad() {
        id = 0;
        idActividad = 0;
        idDetalleActividad = 0;
        idEnc = 0;
        nombre = "";
        checked = "";
        fechaEntrega = "";
        horaIni = "";
        horaFin = "";
        horaIniExtra = "";
        horaFinExtra = "";
        usuario = "";
        np = "";
        dia = 0;
        mes = 0;
        ano = 0;
        minutoReal = 0;
        minutoRealExtra = 0;
        count = 0;
        setCantidadDoc(0);
        numRevisionPlanos = 0;
        setMotivoRevision("");
        setObservacion("");
        encargado = new ModEncargado();
    }

    public int getMinutoReal() {
        return minutoReal;
    }

    public void setMinutoReal(int minutoReal) {
        this.minutoReal = minutoReal;
    }

    public int getMinutoRealExtra() {
        return minutoRealExtra;
    }

    public void setMinutoRealExtra(int minutoRealExtra) {
        this.minutoRealExtra = minutoRealExtra;
    }

    public int getIdEnc() {
        return idEnc;
    }

    public void setIdEnc(int idEnc) {
        this.idEnc = idEnc;
    }

    public int getIdDetalleActividad() {
        return idDetalleActividad;
    }

    public void setIdDetalleActividad(int idDetalleActividad) {
        this.idDetalleActividad = idDetalleActividad;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(String horaIni) {
        this.horaIni = horaIni;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public int getIdActividad() {
        return idActividad;
    }

    public void setIdActividad(int idActividad) {
        this.idActividad = idActividad;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public ModEncargado getEncargado() {
        return encargado;
    }

    public void setEncargado(ModEncargado encargado) {
        this.encargado = encargado;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNp() {
        return np;
    }

    public void setNp(String np) {
        this.np = np;
    }

    public String getHoraIniExtra() {
        return horaIniExtra;
    }

    public void setHoraIniExtra(String horaIniExtra) {
        this.horaIniExtra = horaIniExtra;
    }

    public String getHoraFinExtra() {
        return horaFinExtra;
    }

    public void setHoraFinExtra(String horaFinExtra) {
        this.horaFinExtra = horaFinExtra;
    }

    public int getCantidadDoc() {
        return cantidadDoc;
    }

    public void setCantidadDoc(int cantidadDoc) {
        this.cantidadDoc = cantidadDoc;
    }

    public int getNumRevisionPlanos() {
        return numRevisionPlanos;
    }

    public void setNumRevisionPlanos(int numRevisionPlanos) {
        this.numRevisionPlanos = numRevisionPlanos;
    }

    public String getMotivoRevision() {
        return motivoRevision;
    }

    public void setMotivoRevision(String motivoRevision) {
        this.motivoRevision = motivoRevision;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}

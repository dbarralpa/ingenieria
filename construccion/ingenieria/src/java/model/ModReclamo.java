package model;

import java.util.Date;

public class ModReclamo {

    private long numero;
    private long numeroPlano;
    private String usuario;
    private String observacion;
    private String detencion;
    private String area;
    private Date fechaIngreso;
    private Date fechaAjuste;
    private byte estado;
    private int cantidad;
    private int dias;
    private double orden;
    private double parte;
    private ModMaterial material;

    public ModReclamo() {
        usuario = "";
        detencion = "";
        observacion = "";
        area = "";
        material = new ModMaterial();
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public long getNumeroPlano() {
        return numeroPlano;
    }

    public void setNumeroPlano(long numeroPlano) {
        this.numeroPlano = numeroPlano;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDetencion() {
        return detencion;
    }

    public void setDetencion(String detencion) {
        this.detencion = detencion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaAjuste() {
        return fechaAjuste;
    }

    public void setFechaAjuste(Date fechaAjuste) {
        this.fechaAjuste = fechaAjuste;
    }

    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public double getOrden() {
        return orden;
    }

    public void setOrden(double orden) {
        this.orden = orden;
    }

    public double getParte() {
        return parte;
    }

    public void setParte(double parte) {
        this.parte = parte;
    }

    public ModMaterial getMaterial() {
        return material;
    }

    public void setMaterial(ModMaterial material) {
        this.material = material;
    }

}

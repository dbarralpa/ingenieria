package model;

public class ModEncargado {

    private int id;
    private int user;
    private String checked;
    private String nombreUser;

    public ModEncargado() {
        id = 0;
        user = 0;
        checked = "";
        nombreUser = "";
    }

    public String getNombreUser() {
        return nombreUser;
    }

    public void setNombreUser(String nombreUser) {
        this.nombreUser = nombreUser;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

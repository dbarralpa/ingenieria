package model;

import java.util.ArrayList;

public class ModSesion {
//private String url = "../page/ingenieria.jsp";

    private String url = "../jsp/tabla_1.jsp";
    private int etapaIngenieria = 0;
    private int proceso = 0;
    private int dia = 0;
    private int mes = 0;
    private int ano = 0;
    private int estado = 0;
    private ArrayList datos = new ArrayList();
    private ArrayList equiposIngenieria = new ArrayList();
    private ArrayList procesos = new ArrayList();
    private ArrayList equiposAnalizados = new ArrayList();
    private ArrayList actividades = new ArrayList();
    private ArrayList auxiliar = new ArrayList();
    private ModUsuario usuario = new ModUsuario();
    private ModActividad actividad = new ModActividad();
    /*Mensajes de formulario*/
    private String tip = "";
    private int tipoTip = 0;
    private boolean conTip = false;
    private String valor = "";

    private ModEquipo equipo = new ModEquipo();
    private ArrayList usuarios = new ArrayList();

    public ModSesion() {

    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public ArrayList getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList usuarios) {
        this.usuarios = usuarios;
    }

    public ModEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(ModEquipo equipo) {
        this.equipo = equipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip, int tipoTip) {
        this.setTipoTip(tipoTip);
        this.setConTip(true);
        this.tip = tip;
    }

    public void cleanTip() {
        this.setTipoTip(0);
        this.setConTip(false);
        this.tip = "";
    }

    public boolean isConTip() {
        return conTip;
    }

    public void setConTip(boolean conTip) {
        this.conTip = conTip;
    }

    public int getTipoTip() {
        return tipoTip;
    }

    public void setTipoTip(int tipoTip) {
        this.tipoTip = tipoTip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public ModActividad getActividad() {
        return actividad;
    }

    public void setActividad(ModActividad actividad) {
        this.actividad = actividad;
    }

    public ArrayList getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(ArrayList auxiliar) {
        this.auxiliar = auxiliar;
    }

    public ArrayList getActividades() {
        return actividades;
    }

    public void setActividades(ArrayList actividades) {
        this.actividades = actividades;
    }

    public ArrayList getEquiposAnalizados() {
        return equiposAnalizados;
    }

    public void setEquiposAnalizados(ArrayList equiposAnalizados) {
        this.equiposAnalizados = equiposAnalizados;
    }

    public ModUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(ModUsuario usuario) {
        this.usuario = usuario;
    }

    public int getProceso() {
        return proceso;
    }

    public void setProceso(int proceso) {
        this.proceso = proceso;
    }

    public ArrayList getProcesos() {
        return procesos;
    }

    public void setProcesos(ArrayList procesos) {
        this.procesos = procesos;
    }

    public ArrayList getEquiposIngenieria() {
        return equiposIngenieria;
    }

    public void setEquiposIngenieria(ArrayList equiposIngenieria) {
        this.equiposIngenieria = equiposIngenieria;
    }

    public ArrayList getDatos() {
        return datos;
    }

    public void setDatos(ArrayList datos) {
        this.datos = datos;
    }

    public int getEtapaIngenieria() {
        return etapaIngenieria;
    }

    public void setEtapaIngenieria(int etapaIngenieria) {
        this.etapaIngenieria = etapaIngenieria;
    }

}

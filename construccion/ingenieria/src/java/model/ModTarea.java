
package model;

import java.util.ArrayList;


public class ModTarea {

    private int id;
    private int idTarea;
    private String descripcion;
    private boolean showActividad;
    private ArrayList actividades;

    public ModTarea() {
        id = 0;
        idTarea = 0;
        descripcion = "";
        showActividad = false;
        actividades = new ArrayList();
    }

    public boolean isShowActividad() {
        return showActividad;
    }

    public void setShowActividad(boolean showActividad) {
        this.showActividad = showActividad;
    }

    public ArrayList getActividades() {
        return actividades;
    }

    public void setActividades(ArrayList actividades) {
        this.actividades = actividades;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
   /* public static void main(String[] arg){
       long c=52960l * 60000l; 
       long f = Long.valueOf(c);
        long diffMinutos = Math.abs(f / (60 * 1000));
                    long restominutos = diffMinutos % 60;

                    // calcular la diferencia en horas
                    long diffHoras = (f / (60 * 60 * 1000));
                    System.out.println(String.valueOf(diffHoras + ":" + restominutos));
    }*/
}

package forms;

import controllers.PrmApp;
import controllers.PrmGrabarIngenieria;
import controllers.PrmIngenieria;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModActividad;
import model.ModEquipo;
import model.ModSesion;
import model.ModTarea;
import util.HelperIngenieria;

public class FrmIngenieria extends HttpServlet {

    //public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        ModSesion Sesion = null;
        int volverMac = 0;
        try {
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                Sesion.setUrl("../page/ingenieria.jsp");
                SimpleDateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
                if (req.getParameter("etapaIngenieria1") != null) {
                    Sesion.setEtapaIngenieria(1);
                }
                if (req.getParameter("etapaIngenieria6") != null) {
                    Sesion.setValor("0");
                    Sesion.setDatos(PrmIngenieria.tareasIngenieria());
                    Sesion.setEquiposIngenieria(PrmIngenieria.traerDatosAtividadesTareasAno());
                    Sesion.setProcesos(PrmIngenieria.traerFechaAnos());
                    Sesion.setEtapaIngenieria(6);
                }

                if (req.getParameter("actu") != null) {
                    //LINEA PARA LLENADO EXCEL
                    PrmGrabarIngenieria.actualizarTareas(PrmIngenieria.traerHorasActividadesTareas());
                }
                if (req.getParameter("tarea") != null) {
                    for (int i = 0; i < Sesion.getDatos().size(); i++) {
                        ModTarea tarea = (ModTarea) Sesion.getDatos().get(i);
                        tarea.setShowActividad(false);
                        Sesion.getDatos().set(i, tarea);
                    }
                    ModTarea tarea = (ModTarea) Sesion.getDatos().get(Integer.parseInt(req.getParameter("tarea")));
                    if (tarea.getActividades().isEmpty()) {
                        tarea.setActividades(PrmIngenieria.actividadesPorTarea(tarea.getIdTarea()));
                    }
                    tarea.setShowActividad(true);
                    Sesion.getDatos().set(Integer.parseInt(req.getParameter("tarea")), tarea);
                }

                if (req.getParameter("detalleActividad") != null) {//ddddddd
                    Sesion.setAuxiliar(PrmIngenieria.actividadesDetalle(Integer.parseInt(req.getParameter("id")), Integer.parseInt(req.getParameter("ano")), Integer.parseInt(req.getParameter("mes"))));
                    Sesion.setAno(Integer.parseInt(req.getParameter("ano")));
                    Sesion.setMes(Integer.parseInt(req.getParameter("mes")));
                    Sesion.setEtapaIngenieria(7);
                }

                if (req.getParameter("detalleTarea") != null) {//ddddddd
                    Sesion.setAuxiliar(PrmIngenieria.actividadesDetalleTarea(Integer.parseInt(req.getParameter("id")), Integer.parseInt(req.getParameter("ano")), Integer.parseInt(req.getParameter("mes"))));
                    Sesion.setValor("0");
                    Sesion.setAno(Integer.parseInt(req.getParameter("ano")));
                    Sesion.setMes(Integer.parseInt(req.getParameter("mes")));
                    Sesion.setEtapaIngenieria(7);
                }
                if (req.getParameter("volver") != null) {
                    Sesion.setEtapaIngenieria(6);
                }

                if (req.getParameter("etapaIngenieria8") != null) {
                    Sesion.setProcesos(PrmIngenieria.comboActividades());
                    Sesion.setValor("1");
                    Sesion.setEtapaIngenieria(8);
                }

                if (req.getParameter("insertOrUpdate") != null) {
                    ModActividad acti = null;
                    if (Sesion.getActividad().getIdDetalleActividad() == 0) {
                        acti = new ModActividad();
                    } else {
                        acti = Sesion.getActividad();
                    }
                    acti.setNp(req.getParameter("np"));
                    acti.setIdActividad(Integer.parseInt(req.getParameter("cmbActividad")));
                    acti.setDia(Sesion.getDia());
                    acti.setHoraIni(HelperIngenieria.horaSeteada(req.getParameter("cmbDesdeNormalHora")) + ":" + HelperIngenieria.horaSeteada(req.getParameter("cmbDesdeNormalMinuto")));
                    acti.setHoraFin(HelperIngenieria.horaSeteada(req.getParameter("cmbHastaNormalHora")) + ":" + HelperIngenieria.horaSeteada(req.getParameter("cmbHastaNormalMinuto")));
                    acti.setHoraIniExtra(HelperIngenieria.horaSeteada(req.getParameter("cmbDesdeExtraHora")) + ":" + HelperIngenieria.horaSeteada(req.getParameter("cmbDesdeExtraMinuto")));
                    acti.setHoraFinExtra(HelperIngenieria.horaSeteada(req.getParameter("cmbHastaExtraHora")) + ":" + HelperIngenieria.horaSeteada(req.getParameter("cmbHastaExtraMinuto")));
                    acti.setCantidadDoc(Integer.parseInt(req.getParameter("cmbCantDoc")));
                    acti.setNumRevisionPlanos(Integer.parseInt(req.getParameter("cmbNRevPlanos")));
                    acti.setMotivoRevision(req.getParameter("motRevision"));
                    acti.setObservacion(req.getParameter("observacion"));
                    if (!Sesion.getUsuario().getUsuario().trim().equals("")) {
                        if (Sesion.getActividad().getIdDetalleActividad() == 0) {
                            if (HelperIngenieria.isFechaInvalida(req.getParameter("fec").split("/")[0], req.getParameter("fec").split("/")[2], req.getParameter("fec").split("/")[1], acti.getHoraIni(), acti.getHoraFin())) {
                                Sesion.setTip("La fecha de inicio no puede ser mayor que la fecha de t�rmino", 0);
                            } else if (HelperIngenieria.isFechaInvalida(req.getParameter("fec").split("/")[0], req.getParameter("fec").split("/")[2], req.getParameter("fec").split("/")[1], acti.getHoraIniExtra(), acti.getHoraFinExtra())) {
                                Sesion.setTip("La fecha de inicio no puede ser mayor que la fecha de t�rmino", 0);
                            } else if (PrmGrabarIngenieria.grabarActividadEnc(acti, Integer.parseInt(req.getParameter("fec").split("/")[2]), Integer.parseInt(req.getParameter("fec").split("/")[1]), Sesion.getUsuario().getUsuario(), false)) {
                                Sesion.setTip("Datos insertados", 0);
                            } else {
                                Sesion.setTip("No se pudo insertar", 0);
                            }

                        } else {
                            if (PrmGrabarIngenieria.grabarActividadEnc(acti, Integer.parseInt(req.getParameter("fec").split("/")[2]), Integer.parseInt(req.getParameter("fec").split("/")[1]), Sesion.getUsuario().getUsuario(), true)) {
                                Sesion.setTip("Datos actualizados", 0);
                            } else {
                                Sesion.setTip("No se pudo actualizar", 0);
                            }
                        }
                    } else {
                        Sesion.setTip("Perdi� la sesion, vuelva a entrar", 1);
                    }
                    Sesion.setAuxiliar(PrmIngenieria.actividadesDetallePorUsuario(Sesion.getUsuario().getUsuario(), Integer.parseInt(req.getParameter("fec").split("/")[2]), Integer.parseInt(req.getParameter("fec").split("/")[1]), Integer.parseInt(req.getParameter("fec").split("/")[0])));
                    Sesion.setActividad(new ModActividad());
                }

                if (req.getParameter("setearFecha") != null) {
                    Sesion.setMes(Integer.parseInt(req.getParameter("fec").split("/")[1]));
                    Sesion.setAno(Integer.parseInt(req.getParameter("fec").split("/")[2]));
                    Sesion.setDia(Integer.parseInt(req.getParameter("fec").split("/")[0]));
                    Sesion.setAuxiliar(PrmIngenieria.actividadesDetallePorUsuario(Sesion.getUsuario().getUsuario(), Integer.parseInt(req.getParameter("fec").split("/")[2]), Integer.parseInt(req.getParameter("fec").split("/")[1]), Integer.parseInt(req.getParameter("fec").split("/")[0])));
                    Sesion.setActividad(new ModActividad());
                }

                if (req.getParameter("actualizarActividad") != null) {
                    ModActividad acti = (ModActividad) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("actualizarActividad")));
                    Sesion.setActividad(acti);
                }
                if (req.getParameter("delete") != null) {
                    if (PrmGrabarIngenieria.deleteActividad(Sesion.getActividad())) {
                        Sesion.setActividad(new ModActividad());
                        Sesion.setTip("Datos eliminados", 0);
                    } else {
                        Sesion.setTip("No se pudo eliminar", 0);
                    }
                    Sesion.setAuxiliar(PrmIngenieria.actividadesDetallePorUsuario(Sesion.getUsuario().getUsuario(), Integer.parseInt(req.getParameter("fec").split("/")[2]), Integer.parseInt(req.getParameter("fec").split("/")[1]), Integer.parseInt(req.getParameter("fec").split("/")[0])));
                }

                if (req.getParameter("limpiar") != null) {
                    Sesion.setActividad(new ModActividad());
                }

                if (req.getParameter("volverMac") != null) {
                    volverMac = Integer.parseInt(req.getParameter("volverMac"));
                }

                if (req.getParameter("etapaIngenieria9") != null) {
                    Sesion.setAuxiliar(PrmIngenieria.reclamosPorPlanos());
                    Sesion.setEtapaIngenieria(9);
                }

                if (req.getParameter("actualizarEstadoReclamo") != null) {
                    ModEquipo equi = (ModEquipo) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("actualizarEstadoReclamo")));
                    Sesion.setEquipo(equi);
                    Sesion.setUsuarios(PrmIngenieria.proyectista());
                    Sesion.setEstado(Integer.parseInt(req.getParameter("esta")));
                    Sesion.setEtapaIngenieria(10);
                }
                if (req.getParameter("actualizarReclamo") != null) {
                    if (!req.getParameter("observacion").trim().equals("")) {
                        if (Sesion.getEstado() == 2) {
                            if (!req.getParameter("cmbProyectistas").equals("-1")) {
                                if (PrmGrabarIngenieria.actualizarReclamo(req.getParameter("observacion"), Sesion.getUsuario().getUsuario(), Sesion.getEquipo().getReclamo().getNumero(), Sesion.getEstado(), req.getParameter("cmbProyectistas"))) {
                                    Sesion.setTip("Pedido actualizado", 2);
                                } else {
                                    Sesion.setTip("No se pudo actualizar", 1);
                                }
                            } else {
                                Sesion.setTip("Debe poner  al proyectista", 1);
                            }
                        }
                        if (Sesion.getEstado() == 3) {
                            if (PrmGrabarIngenieria.actualizarReclamo(req.getParameter("observacion"), Sesion.getUsuario().getUsuario(), Sesion.getEquipo().getReclamo().getNumero(), Sesion.getEstado(), req.getParameter("cmbProyectistas"))) {
                                Sesion.setTip("Pedido actualizado", 2);
                            } else {
                                Sesion.setTip("No se pudo actualizar", 1);
                            }
                        }
                    } else {
                        Sesion.setTip("Debe llenar el campo observaci�n", 1);
                    }
                }

            }
        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }

        req.getSession().setAttribute("Sesion", Sesion);
        if (volverMac == 1) {
            res.sendRedirect("/mac/jsp/dependencias/Ingenieria.jsp?idSesion=" + Sesion.getUsuario().getIdSesion() + "&usuario=" + Sesion.getUsuario().getUsuario() + "&tipou=" + Sesion.getUsuario().getTipoUsuario());
        } else if (volverMac == 2) {
            res.sendRedirect("/mac/jsp/dependencias/Produccion.jsp?idSesion=" + Sesion.getUsuario().getIdSesion() + "&usuario=" + Sesion.getUsuario().getUsuario() + "&tipou=" + Sesion.getUsuario().getTipoUsuario());
        } else {
            res.sendRedirect("template/appLayout.jsp");
        }

        //super.SendtoHome(res, req);
        //return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}

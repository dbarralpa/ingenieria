package forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModActividad;
import model.ModSesion;
import org.json.JSONArray;
import org.json.JSONObject;
import util.HelperIngenieria;

public class FrmJson extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ModSesion Sesion = null;
        try {
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                JSONObject result = new JSONObject();
                JSONArray array = new JSONArray();
                int amount = 10;
                int start = 0;
                int echo = 0;
                int col = 0;

                String dir = "asc";
                String sStart = req.getParameter("iDisplayStart");
                String sAmount = req.getParameter("iDisplayLength");
                String sEcho = req.getParameter("sEcho");
                String sCol = req.getParameter("iSortCol_0");
                String sdir = req.getParameter("sSortDir_0");

                String buscar = req.getParameter("sSearch");

                if (sStart != null) {
                    start = Integer.parseInt(sStart);
                    if (start < 0) {
                        start = 0;
                    }
                }
                if (sAmount != null) {
                    amount = Integer.parseInt(sAmount);
                    if (amount == -1) {
                        amount = Sesion.getAuxiliar().size();
                    }
                }
                if (sEcho != null) {
                    echo = Integer.parseInt(sEcho);
                }
                if (sCol != null) {
                    col = Integer.parseInt(sCol);
                    if (col < 0 || col > 13) {
                        col = 0;
                    }
                }
                if (sdir != null) {
                    if (!sdir.equals("asc")) {
                        dir = "desc";
                    }
                }
                int total = Sesion.getAuxiliar().size();
                int totalAfterFilter = total;
                result.put("sEcho", echo);
                ArrayList coincidencias = new ArrayList();
                ArrayList actividades = new ArrayList();  
                for (int i = 0; i < total; i++) {
                    ModActividad acti = (ModActividad) Sesion.getAuxiliar().get(i);
                    acti.getNombre();
                    acti.getNp();
                    acti.getDia();
                    acti.getHoraIni();
                    acti.getHoraFin();
                    HelperIngenieria.devolverHoras(acti.getMinutoReal());
                    acti.getHoraIniExtra();
                    acti.getHoraFinExtra();
                    HelperIngenieria.devolverHoras(acti.getMinutoRealExtra());
                    acti.getCantidadDoc();
                    acti.getNumRevisionPlanos();
                    acti.getMotivoRevision().toLowerCase();
                    acti.getObservacion().toLowerCase();
                    actividades.add(acti);
                }
                actividades = HelperIngenieria.ordenarArreglo(actividades, col, dir);                
                if (!buscar.equals("")) {
                    totalAfterFilter = 0;
                    for (int i = 0; i < total; i++) {
                        ModActividad acti = (ModActividad) actividades.get(i);
                        if (acti.getNombre() != null && !buscar.equals("")) {
                            if (HelperIngenieria.buscaCoincidencia(acti, buscar)) {
                                totalAfterFilter++;
                                acti.getNombre();
                                acti.getNp();
                                acti.getDia();
                                acti.getHoraIni();
                                acti.getHoraFin();
                                HelperIngenieria.devolverHoras(acti.getMinutoReal());
                                acti.getHoraIniExtra();
                                acti.getHoraFinExtra();
                                HelperIngenieria.devolverHoras(acti.getMinutoRealExtra());
                                acti.getCantidadDoc();
                                acti.getNumRevisionPlanos();
                                acti.getMotivoRevision().toLowerCase();
                                acti.getObservacion().toLowerCase();
                                coincidencias.add(acti);
                            }
                        }
                    }
                    coincidencias = HelperIngenieria.ordenarArreglo(coincidencias, col, dir); 
                    for (int a = start; a < start + amount; a++) {
                        if (a < coincidencias.size()) {
                            ModActividad act = (ModActividad) coincidencias.get(a);
                            JSONArray ja = new JSONArray();
                            ja.put(("<a href='../FrmIngenieria?actualizarActividad=" + a + "'/>" + act.getNombre() + "</a>"));                                                  
                            ja.put(act.getNp());
                            ja.put(act.getDia());
                            ja.put(act.getHoraIni());
                            ja.put(act.getHoraFin());
                            ja.put(HelperIngenieria.devolverHoras(act.getMinutoReal()));
                            ja.put(act.getHoraIniExtra());
                            ja.put(act.getHoraFinExtra());
                            ja.put(HelperIngenieria.devolverHoras(act.getMinutoRealExtra()));
                            ja.put(act.getCantidadDoc());
                            ja.put(act.getNumRevisionPlanos());
                            ja.put(act.getMotivoRevision().toLowerCase());
                            ja.put(act.getObservacion().toLowerCase());
                            array.put(ja);
                        }
                    }
                } else {
                    for (int a = start; a < start + amount; a++) {
                        if (a < actividades.size()) {
                            ModActividad act = (ModActividad) actividades.get(a);
                            JSONArray ja = new JSONArray();
                            ja.put(("<a href='../FrmIngenieria?actualizarActividad=" + a + "'/>" + act.getNombre() + "</a>"));                            
                            ja.put(act.getNp());
                            ja.put(act.getDia());
                            ja.put(act.getHoraIni());
                            ja.put(act.getHoraFin());
                            ja.put(HelperIngenieria.devolverHoras(act.getMinutoReal()));
                            ja.put(act.getHoraIniExtra());
                            ja.put(act.getHoraFinExtra());
                            ja.put(HelperIngenieria.devolverHoras(act.getMinutoRealExtra()));
                            ja.put(act.getCantidadDoc());
                            ja.put(act.getNumRevisionPlanos());
                            ja.put(act.getMotivoRevision().toLowerCase());
                            ja.put(act.getObservacion().toLowerCase());
                            array.put(ja);
                        }
                    }
                }

                result.put(
                        "iTotalRecords", total);
                result.put(
                        "iTotalDisplayRecords", totalAfterFilter);
                result.put(
                        "aaData", array);
                response.setContentType(
                        "application/json");
                response.setHeader(
                        "Cache-Control", "no-store");
                out.print(result);
            }
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion"    scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"        scope="page"    class="controllers.PrmApp" />

<%
    //response.setDateHeader("Expires", 0);
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0,max-age=0,max-stale = 0'");
    //response.setHeader("Pragma", "no-cache");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <!-- TITULO -->
        <%--title><bean:message key="title.main"/></title--%>

        <!-- HOJAS DE ESTILO -->
        <link rel="stylesheet" href="../css/body.css" type="text/css"/>
        <link rel="stylesheet" href="../css/table.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../css/notification.css" />
        <link rel="stylesheet" type="text/css" href="../css/loading.css" />

        <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="../js/overlib.js"></script>
        <script type="text/javascript" src="../js/loading.js"></script> 
        <script type="text/javascript" src="../js/mensajes.js"></script>





        <!-- JAVASCRIPT -->
    </head>
    <body>
        <%request.setAttribute("LOAD_ONSUBMIT", "onsubmit=\"javascript:loading();\"");
            request.setAttribute("LOAD_ONCLICK", "onclick=\"javascript:loading();\"");%>
        <div id="overLoading" style="display:none;"><div id="mensajeLoading"><table width="200"></table></div></div>
        <div id="page">
            <table width="100%" cellpadding="0" cellspacing="0">

                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0">                
                            <tr>
                                <td>
                                    <%try {%>

                                    <jsp:include page="<%=Sesion.getUrl()%>" >
                                        <jsp:param name="" value=""/>
                                    </jsp:include>

                                    <%} catch (Exception ex) {
                                        vw.addError(ex, true);%>ERROR SECCION<%}
                                    %>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>        
            </table>          
        </div>
    </body>
</html>
<%if (Sesion.isConTip()) {%>
<script type="text/javascript">
    OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>

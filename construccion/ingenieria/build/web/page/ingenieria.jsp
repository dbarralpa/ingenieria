<%@page import="util.HelperIngenieria"%>
<%@page import="controllers.PrmApp"%>
<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="LOAD_ONCLICK" scope="request" class="java.lang.String" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="view.VwIngenieria" />
<%response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0,max-age=0");%>
<script type="text/javascript" src="../js/popcalendar.js"></script>
<script type="text/javascript" src="../js/initcalendar.js"></script>
<form action="../FrmIngenieria" method="post" <%=LOAD_ONSUBMIT%> name="FrmIngenieria">
    <script>
        $(document).ready(function() {
            $("form").bind("keypress", function(e) {
                if (e.keyCode == 13) {
                    $("form").submit();
                }
            });
        });

        function actividad() {
            if (document.FrmIngenieria.cmbActividad.value == '-1' || document.FrmIngenieria.cmbDesdeNormalHora.value == '0' || document.FrmIngenieria.cmbHastaNormalHora.value == '0' || document.FrmIngenieria.fec.value == '00/00/00') {
                alert('Tiene que elegir una actividad, hora inicio, hora fin y fecha');
                return false;
            } else {
                return true;
            }

        }

        function actualizarReclamo() {
            if (document.FrmIngenieria.observacion.value == '') {
                alert('Debe colocar todos los datos');
                return false;
            } else {
                return true;
            }

        }
    </script>
    <input name="frm" type="hidden" value="ingenieria"/>
    <table cellpadding='0' cellspacing='0' width="100%">
        <%if (Sesion.getEtapaIngenieria() == 1) {%>
        <tr>
            <td style="padding-top: 20px">
                <table align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo10">
                            MODULO ASOCIADO A MRP
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 20px"><img src="../img/bolaRoja.gif" height="15" width="16"><a href="../FrmIngenieria?etapaIngenieria9=SI"><font class="Estilo9">&nbsp;Atrasos de Equipos por planos</font></a></td>
                    </tr>
                    <tr>
                        <td ><img src="../img/bolaRoja.gif" height="15" width="16"><a href="../FrmIngenieria?etapaIngenieria6=SI"><font class="Estilo9">&nbsp;Resumen labores diarias</font></a></td>
                    </tr>
                    <tr>
                        <td align='right' style="padding-top: 30px"><a href='../FrmIngenieria?volverMac=2'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <%}
            if (Sesion.getEtapaIngenieria() == 6) {%>
        <tr>
            <td colspan="11" style="padding-top: 10px" class="Estilo10" align="center">INGRESO Y REPORTE DE ACTIVIDADES</td>
        </tr>
        <%=vw.grillaTareas(request)%>
        <td style="padding-top: 20px"><a href='../FrmIngenieria?etapaIngenieria1=yes'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
                <%}%>


        <%if (Sesion.getEtapaIngenieria() == 7) {%>
        <tr>
            <td style="padding-top: 20px">

                <jsp:include page="../jsp/tabla.jsp">
                    <jsp:param name="val" value="0"/>
                </jsp:include> 
            </td>
        </tr>
        <tr>
            <td align='right' style="padding-top: 10px"><a href='../FrmIngenieria?volver=0'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
        </tr>

        <%}
            if (Sesion.getEtapaIngenieria() == 8) {%>
        <tr>
            <td colspan="11" style="padding-top: 10px" class="Estilo10" align="center">REPORTE DE ACTIVIDADES</td>
        </tr>

        <%//=vw.AnoMesDia(request)%>
        <tr>
            <td style="padding-top: 20px">
                <table>
                    <tr>
                        <td height="20" class="Estilo1">Fecha ingreso</td>
                        <td align="right">:</td>
                        <%String fecha = HelperIngenieria.horaSeteada(String.valueOf(Sesion.getDia())) + "/" + HelperIngenieria.horaSeteada(String.valueOf(Sesion.getMes())) + "/" + HelperIngenieria.horaSeteada(String.valueOf(Sesion.getAno()));%>
                        <td align="right"><input id="fec" name="fec" type="text" size="15" readonly class="txtFecha" /></td>
                        <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fec'));" /></td>
                        <td>&nbsp;&nbsp;<input type="submit" name="setearFecha" id="setearFecha" value="Buscar registros" class="Estilo10"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px">
                <table border="1" cellpadding="0" cellspacing="0" class="Contenido" width="100%">
                    <!--tr>
                        <td colspan="11" class="Titulo" align="center">Ingreso de actividades</td>
                    </tr-->
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2" align="center">HORAS NORMALES</td>
                        <td colspan="2" align="center">HORAS EXTRAS</td>
                        <td colspan="4">&nbsp;</td>

                    </tr>
                    <tr>
                        <td align="center">NP</td>
                        <td align="center">ACTIVIDAD</td>
                        <td align="center">DESDE</td>
                        <td align="center">HASTA</td>
                        <td align="center">DESDE</td>
                        <td align="center">HASTA</td>
                        <td align="center">CANT DOC</td>
                        <td align="center">N� REV. P</td>
                        <td align="center">MOTIVO REVISI�N</td>
                        <td align="center">OBSERVACIONES</td>
                    </tr>
                    <%=vw.grillaIngresarActividades(request)%>
                </table>
            </td>
        </tr>
        <tr>
            <td  align="right">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><input type="submit" class="Estilo10" name="limpiar" id="limpiar"  value="Limpiar Actividad">&nbsp;&nbsp;</td>
                        <td><input type="submit" class="Estilo10" name="delete" id="delete"  value="Borrar Actividad">&nbsp;&nbsp;</td>
                        <td><input type="submit" class="Estilo10" onclick="return actividad()" name="insertOrUpdate" id="insertOrUpdate"  value="Insertar o editar Actividad"></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="padding-top: 20px">

                <jsp:include page="../jsp/tabla.jsp">
                    <jsp:param name="val" value="1"/>
                </jsp:include> 
            </td>
        </tr>
        <tr>
            <td align='right' style="padding-top: 10px"><a href='../FrmIngenieria?volverMac=1'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
        </tr>

        <%}
            if (Sesion.getEtapaIngenieria() == 9) {%>
        <tr>
            <td style="padding-top: 10px" class="Estilo10" align="center">RECLAMOS DE EQUIPO POR PLANOS</td>
        </tr>
        <%=vw.grillaReclamosPorPlanos(request)%>
        <tr>
            <td style="padding-top: 20px"><a href='../FrmIngenieria?etapaIngenieria1=yes'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
        </tr>
        <%}
            if (Sesion.getEtapaIngenieria() == 10) {%>
        <tr>
            <td style="padding-top: 10px" class="Estilo10" align="center">ACTUALIZAR ESTADO RECLAMO</td>
        </tr>
        <%=vw.grillaReclamosPorPlanosEquipo(request)%>
        <tr>
            <td style="padding-top: 20px">
                <table>
                    <!--td>
                        <table>
                            <tr>
                                <td height="20" class="Estilo1">Fecha ajuste</td>
                                <td align="right">:</td>
                                <td align="right"><input id="fec" name="fec" type="text" size="15" readonly class="txtFecha" /></td>
                                <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fec'));" /></td>

                            </tr>
                        </table>
                    </td-->
                    <td class="Estilo1">
                        &nbsp;Observaci�n:&nbsp;
                    </td>
                    <td class="Estilo1">
                        <textarea name="observacion" id="observacion" rows="1" cols="20"></textarea>
                    </td>
                    <td>&nbsp;&nbsp;<input type="submit" onclick="return actualizarReclamo()" name="actualizarReclamo" id="actualizarReclamo" value="Actualizar Reclamo" class="Estilo10"></td>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px"><a href='../FrmIngenieria?etapaIngenieria9=yes'><img src='../img/volver2.jpg' border='0' width='89' height='18' alt='volver2'/></a></td>
        </tr>

        <%}%>
    </table>
</form>

<%@page import="util.HelperIngenieria"%>
<%@page import="controllers.PrmApp"%>
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="view.VwIngenieria" />
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />


        <title>DataTables example</title>
        <style type="text/css" title="currentStyle" >
            @import "../css/demo_page.css";
            @import "../css/demo_table.css";
            @import "../css/demo_table_jui.css";
            @import "../css/jquery-ui-1.8.4.custom.css";
            @import "../css/tableTools.css";
        </style>
        <script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="../js/ZeroClipboard.js"></script>
        <script type="text/javascript" language="javascript" src="../js/dataTables.tableTools.js"></script>
        <script type="text/javascript" >
            $(document).ready(function() {

                $('#example').dataTable({
                    "sPaginationType": "full_numbers",
                    "sScrollY": "400px",
                    "bPaginate": true,
                    "bScrollCollapse": true,
                    "sScrollX": "100%",
                    //"sScrollY": "300px",
                    //"sScrollX": "100%",
                    //"sScrollXInner": "150%",
                    //"bScrollCollapse": true,

                    "oLanguage": {
                        "sLengthMenu": 'Display <select>' +
                                '<option value="10">10</option>' +
                                '<option value="30">25</option>' +
                                '<option value="50">50</option>' +
                                '<option value="-1">TODOS</option>' +
                                '</select> registros',
                        /*"oPaginate": {
                         "sFirst": "|<"
                         , "sPrevious": "<"
                         , "sNext": ">"
                         , "sLast": ">|"
                         },*/
                        /* "sLengthMenu": "Muestra _MENU_ registros por p�gina",*/
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                        "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                        "sSearch": "Buscar"

                    },
                    "sDom": 'T<"clear">lfrtip',
                    "oTableTools": {
                        "sSwfPath": "../js/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            "xls",
                            {
                                "sExtends": "pdf",
                                "sPdfOrientation": "landscape",
                                "sPdfMessage": "Your custom message would go here."
                            }
                        ]
                    },
                    "aoColumns": [
                        {"sClass": "center", "aTargets": [0]},
                        {"sClass": "center", "aTargets": [1]},
                        {"sClass": "center", "aTargets": [2]},
                        {"sClass": "center", "aTargets": [3]},
                        {"sClass": "center", "aTargets": [4]},
                        {"sClass": "center", "aTargets": [5]},
                        {"sClass": "center", "aTargets": [6]},
                        {"sClass": "center", "aTargets": [7]},
                        {"sClass": "center", "aTargets": [8]},
                        {"sClass": "center", "aTargets": [9]},
                        {"sClass": "center", "aTargets": [10]},
                        {"sClass": "center", "aTargets": [11]},
                        {"sClass": "center", "aTargets": [12]}
                    ],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "../FrmJson"
                });
            });
        </script>
    </head>
    <body>
        <!--div id="dt_example"-->
        <div id="container">
            <div id="dynamic">
                <%if (Sesion.getValor().equals("0")) {%>
                <center class="Estilo10">A�o:&nbsp;<%=Sesion.getAno()%>&nbsp;&nbsp;&nbsp;Mes:&nbsp;<%=HelperIngenieria.mes(Sesion.getMes())%></center>
                    <%}%>

                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                    <thead>
                        <tr>
                            <%if (Sesion.getValor().equals("0")) {%>
                            <th>Nombre usuario</th>
                                <%} else if (Sesion.getValor().equals("1")) {%>
                            <th>Actividad</th>
                                <%}%>
                                <%=vw.encabezadoActividades(request)%>
                        </tr>
                    </thead>
                    <tbody>
                    <td colspan="13" class="dataTables_empty">Loading data from server</td>
                    <%//=vw.grillaActividades(request)%>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!--/div-->
    </body>
</html>

